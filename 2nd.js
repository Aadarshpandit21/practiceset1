
const items = {
    item_392019302: {
        name: "Washing Machine",
        stock: 3,
    },
    item_392019342: {
        name: "Light Bulb",
        stock: 3,
    },
    item_392019340: {
        name: "Streaming Device",
        stock: 2
    },
    item_392019389: {
        name: "Plug",
        stock: 1
    },
    item_392019311: {
        name: "Trace",
        stock: 1
    }
}


// question 1;
// const result = Object.keys(items).reduce((acc,curr) =>{
//     acc[items[curr].name] = {}
//     acc[items[curr].name].item_id = curr;
//     acc[items[curr].name].stock = items[curr].stock
  
//     return acc;
//   },{})
//     console.log(result);

// question1 solution by using for loop

// let result ={};
// for(let key in items){
//     const iD = items[key].name;
//     if(result[iD]){
//         result[iD][key]=items[key]
//     }
//     else{
//             result[iD]={}
//             result[iD].item_id= key;
//             result[iD].stock = items[key].stock;
//         }

    
// }
// console.log(result)

// Ques 2
 
// function addProperty(itemsObject,propertyName,value){
//     //console.log(itemsObject,propertyName,value);
//     Object.keys(itemsObject).forEach((ele)=>{
//          itemsObject[ele][propertyName]=value;
//     })
//     console.log(itemsObject);
  
//   }
//   addProperty(items,'colour','red')

//ques 3 
// const clone1 = Object.assign({},items);
// console.log(clone1);
// const clone2 = {...items};
// console.log(clone2);
 
/*
Q1.Form the following solution

const result = {
    washing_machine: {
        item_id: 'item_392019302',
        stock: 3
    },
    light_bulb: {
        item_id: 'item_392019342',
        stock: 3
    },
    ...
}



Q.2 Write a function that takes items object,  propertyName and value as parameters .
The propertyName is added to each Object.
Try not to mutate the original object.

Q3. Clone items properly 
using object.assign
using spread operator.
// */
