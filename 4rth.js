const data = [
    { 
        place: "Melbourne", 
        country: "Australia", 
        location: {
            lat: '99',
            lng: '88'
        },
        temperature: '38 Degree Celsius'
    },{ 
        place: "New Delhi", 
        country: "India", 
        location: {
            lat: '84',
            lng: '44'
        },
        temperature: '42 Degree Celsius'
    },{ 
        place: "Pretoria", 
        country: "SouthAfrica", 
        location: {
            lat: '35',
            lng: '24'
        },
        temperature: '39 Degree Celsius'
    },{ 
        place: "Mexico City", 
        country: "Mexico", 
        location: {
            lat: '34',
            lng: '38'
        },
        temperature: '43 Degree Celsius'
    },
    { 
        place: "London", 
        country: "England", 
        location: {
            lat: '57',
            lng: '34'
        },
        temperature: '26 Degree Celsius'
    }
]
//// Question 1:-
const result = data.reduce((acc,cum) =>{
      const obj = {};
      obj.place = cum.location;
     
      acc.push(obj);
      return acc;
    

},[])
 console.log(result);
 console.log(data); ///original data

// Question 2:-
const clone = data.slice(0);
 const result1 = clone.sort((a,b)=>{
    return parseInt(a.temperature.split(' ')) - parseInt(b.temperature.split(' '));
 })
console.log(result1);
console.log(data);  /// original data

///Question 3 :-
const result2 = data.reduce((acc, cum) =>{
    const obj = {country: {place: {}}};
    const locationObj = {};
    locationObj.lat = cum.location.lat;
    locationObj.lng = cum.location.lng;
    obj.country.place.location = locationObj;
    obj.country.temperature = cum.temperature;
   acc.push(obj);
    return acc;
  }, []);
  console.log(result2);
  console.log(result2[0].country.place);
  console.log(data);   // original data




//// Question 4;/// by using map:-

//  const result3 = data.map((ele) =>{
//      if(ele.country==="SouthAfrica"){
//         ele.temperature =  "49 Degree Celsius";
         
//      }
//      return ele;
//  })
//  console.log(result3);

 /// Question 4:- by using reduce

//     const result4 = data.reduce((acc,cum) =>{
//       if(cum.country==="SouthAfrica"){
//          cum.temperature =  "49 Degree Celsius";
 
//       } 
//       acc.push(cum);
//       return acc;
//   },[]);
//   console.log(result4);


  //// Question 5 :-
  
   const obj = { 
    place: "Bangalore", 
    country: "India", 
    location: {
        lat: '84',
        lng: '47'
    },
    temperature: '29 Degree Celsius'
};

const result5 = data.slice(0);
 result5.splice(3,0,obj);
 console.log(result5);    
 console.log(data);    /// original array
  
 //Question 6 :-

 const result6 = data.slice(0);
 result6.splice(2,1);
 console.log(result6);
 console.log(data);       // original array
  
 //Question 7:-

 const result7 = data.slice(0);
 const secondElement = result7.splice(1,1);
 const secondLastElement = result7.splice(result7.length-2,1);
 
   result7.splice(1,0,secondLastElement[0]);
   
   result7.splice(result7.length-1,0,secondElement[0]);
   console.log(result7);
   console.log(data);                      /// original data


/*
Note: Make sure not to mutate the array in all of the following questions.


Q1. Get all latitude and longitude of all the places in the following manner.
[{place: (lat, long)}, ...]

Q2. Sort data based on temperature (Low temperature first).

Q3.Rearrange data in the following format
[{ country: { place: { location: {lat, lng }, temperature }}}, ...]

Q4. Change temperature of SouthAfrica "Pretoria" to "49 Degree Celsius".

Q5. Add a new Object in the fourth postiion.
{ 
        place: "Bangalore", 
        country: "India", 
        location: {
            lat: '84',
            lng: '47'
        },
        temperature: '29 Degree Celsius'
 }

 Q6. Delete the third element in the array .
 Q7. Swap elements at position 2 and second last.
*/