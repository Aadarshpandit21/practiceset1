const school = {
  "engineering": {
    "students": 324,
    "dep_id": 3,
    "faculties": 4
  },
  "medical": {
    "students": 499,
    "dep_id": 2,
    "faculties": 6
  },
  "pure-science": {
    "students": 133,
    "dep_id": 1,
    "faculties": 2
  },
  "linguistics": {
    "students": 183,
    "dep_id": 4,
    "faculties": 3
  },
  "philosophy": {
    "students": 73,
    "dep_id": 5,
    "faculties": 2
  }
}
   

     const clone = {...school}; 
  
     // 1. Group the data in terms of deparment id.
      const modifiedSchool = Object.keys(school).reduce((acc, curr) => {
           acc[school[curr].dep_id] = school[curr]
          // console.log(acc);
           return acc;
      }, {})
      console.log(modifiedSchool);
     // 2. Filter data with students greater than 200.
     let result = {};
      Object.keys(clone).forEach((ele) =>{
      
        if( clone[ele].students>200){
           result[ele]=clone[ele];
        };
        
      });
      console.log(result);
      
      
      //3. Add a new property (labs-required) to each object . Set value as true for engineering medical and purescience ..and 
      //false for lingustics and philosophy
       
         
             Object.keys(clone).forEach((ele)=>{
            if(ele==='engineering'||ele==='medical'||ele==='pure-science'){
                   clone[ele].labs_required = 'true';
            }else{
                 clone[ele].labs_required = 'false';
            }
          
         })
         
          console.log(clone);

           // console.log(school);